function getCube (num){
	return num**3;
};

console.log(`The cube of 2 is ${getCube(2)}`);

const address = ['258 Washington Ave Nw', 'California', '90011'];
const [city, state, zip] = address;

console.log(`I live at ${city}, ${state} ${zip}`);


const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	measurement: {
		length: 20,
		width: 3
	}

}

const {name, type, weight, measurement: {length, width}} = animal;


console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${length} ft ${width} in.`);


const numbers = [1,2,3,4,5];
function looper (x, y) {
	return x + y;

}

numbers.forEach(num => console.log(num));

const sum = numbers.reduce((x,y) => x + y);
console.log(sum);



